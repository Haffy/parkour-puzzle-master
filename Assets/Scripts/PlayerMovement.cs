using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public enum PlayerStates
    {
        Grounded,//on ground
        InAir, //in air
        OnWalls, //running on the walls
        LedgeGrab, //pulling up a ledge
    }

    private PlayerCollision Coli;
    private Rigidbody Rigid;
    
    

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    [Header("Physics")]
    public float MaxSpeed; //how fast we are running forward
    public float BackwardsSpeed; //how fast we are running backwards
    public float InAirControl; //how much movement control in air

    private float ActSpeed; //speed is applied to the rigidbody
    public float Acceleration; //how fast we increase speed
    public float Decceleration; //how quickly we slow down
    public float DirectionControl = 8; //how much directional control we have
    public PlayerStates CurrentState; //what current state the player is in
    private float InAirTimer; //Air timer, useful when falling or on walls
    private float OnGroundTimer;
    private float AdjustmentAmt; //the amount of acceleration to add


    [Header("Turning")]
    public float TurnSpeed; //how fast we turn when on the ground
    public float TurnSpeedInAir; //how fast we turn when in air
    public float TurnSpeedOnWalls; //how fast we turn when on the walls
    public float LookUpSpeed; //how fast we look up and down
    public Camera Head; //what will function as our players head to tilt up and down 
    private float YTurn; //how much we have turned left and right
    private float XTurn; //how much we have turned Up or Down
    public float MaxLookAngle = 65; //how much we can look up
    public float MinLookAngle = -30; //how much we can look down
    public float mouseSensitivity = 200f;

    [Header("Jumping")]
    public float JumpHeight; //how high we can jump

    [Header("Wall Runs")]
    public float WallRunTime = 2f; //how long we can run on walls
    private float ActWallRunTime = 5f; //how long we are actually on a wall
    public float TimeBeforeWallRun = 0.2f; //how long we have to be in the air before we can wallrun
    public float WallRunUpwardsMovement = 2f; //how much we move up a wall when running on it. 0 = slowly move down wall
    public float WallRunSpeedAcceleration = 2f; //how fast we build speed to run up walls

    

   
    [Header("WallGrabbing")]
    public float PullUpTime; //the time it takes to pull onto a ledge
    private float ActPullTm; //the actual time it takes to pull up a ledge
    private Vector3 OrigPos; //the original Position before grabbing a ledge
    private Vector3 LedgePos; //the ledge position to move to

       // Start is called before the first frame update
    void Start()
    {
        Coli = GetComponent<PlayerCollision>();
        Rigid = GetComponent<Rigidbody>();
        
      
          
        
        AdjustmentAmt = 1;
    }

    // Update is called once per frame
    void Update()
    {

        float XMove = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float YMove = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;



        if (CurrentState == PlayerStates.Grounded)
        {
            //if jump is pressed
            if (Input.GetKeyDown (KeyCode.Space))
            {
                //jump upwards
                JumpUp();
            }
        }
        else if (CurrentState == PlayerStates.InAir)
        {
            //check for ledge grabs
            if (Input.GetKeyDown (KeyCode.E))
            {
                Vector3 LedgePos = Coli.CheckLedges();
                if (LedgePos != Vector3.zero)
                {
                    LedgeGrab(LedgePos);
                }
            }

            //Check if there is a wall to run on
            bool Wall = CheckWalls(XMove, YMove);

            //we are on the wall
            if (Wall)
            {
                if (InAirTimer > TimeBeforeWallRun)
                {
                    SetOnWall();
                    return;
                }
            }

            //check for the ground 
            bool Grounded = Coli.CheckFloor(-transform.up);

            //we are on the ground after being in air, stops us being able to constantly jump
            if (Grounded && InAirTimer > 0.5f)
            {
                SetOnGround();
            }
        }
        else if (CurrentState == PlayerStates.OnWalls)
        {
            //check for ledge grabs
            if (Input.GetKeyDown (KeyCode.E))
            {
                Vector3 LedgePos = Coli.CheckLedges();

                if (LedgePos != Vector3.zero)
                {
                    LedgeGrab(LedgePos);
                }
            }

            //Check if there is a wall to run on
            bool Wall = CheckWalls(XMove, YMove);

            //we are no longer on the wall, fall off it
            if (!Wall)
            {
                SetInAir();
                return;
            }

            //check for the ground 
            bool Grounded = (CurrentState == PlayerStates.Grounded);

            //we are on the ground
            if (Grounded)
            {
                SetOnGround();
            }
        }
        else if (CurrentState == PlayerStates.LedgeGrab)
        {
            //clamp our rigid velocity to nothing
            Rigid.velocity = Vector3.zero;
        }

        
    }

    

    private void FixedUpdate()
    {
        float Del = Time.deltaTime;

        //get our players rotation amount
        float CamX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float CamY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        //look up and down
        LookUpDown(CamY, Del);

    
        //get inputs
        float horInput = Input.GetAxis("Horizontal");
        float verInput = Input.GetAxis("Vertical");

        if (CurrentState == PlayerStates.Grounded)
        {
            //tick our ground timer
            if (OnGroundTimer < 10)
                OnGroundTimer += Del;


            //get magnituded of our inputs
            float InputMagnitude = new Vector2(horInput, verInput).normalized.magnitude;
            //get the amount of speed, based on if we press forwards or backwards
            float TargetSpd = Mathf.Lerp(BackwardsSpeed, MaxSpeed, verInput); //using the vertical input as a lerp from if forward is being pressed
            

            LerpSpeed(InputMagnitude, Del, TargetSpd);

            MovePlayer(horInput, verInput, Del);
            TurnPlayer(CamX, Del, TurnSpeed);

         

            //check for the ground 
            bool Grounded = Coli.CheckFloor(-transform.up);

            //we are in the air
            if (!Grounded)
            {
                if (InAirTimer < 0.2f)
                    InAirTimer += Del;
                else
                {
                    SetInAir();
                    return;
                }
            }
            else
            {
                //we are on the ground to remove any increase in the air timer
                InAirTimer = 0;
            }
        }
        else if (CurrentState == PlayerStates.InAir)
        {
            //tick our Air timer
            if (InAirTimer < 10)
                InAirTimer += Del;

            MoveInAir(horInput, verInput, Del);

            //turn our player with the in air modifier
            TurnPlayer(CamX, Del, TurnSpeedInAir);
        }
        else if (CurrentState == PlayerStates.OnWalls)
        {
            //tick our wall run timer
            ActWallRunTime += Del;
          
            //turn our player with the in air modifier
            TurnPlayer(CamX, Del, TurnSpeedOnWalls);

            //move our player when on a wall
            WallMove(verInput, Del);
        }
        else if (CurrentState == PlayerStates.LedgeGrab)
        {
            //tick ledge grab time 
            ActPullTm += Del;

            //pull up the ledge
            float PullUpLerp = ActPullTm / PullUpTime;

            if (PullUpLerp < 0.5)
            {
                //lerp our player to the ldeges y position
                float LAmt = PullUpLerp * 2;
                transform.position = Vector3.Lerp(OrigPos, new Vector3(OrigPos.x, LedgePos.y , OrigPos.z), LAmt);
            }
            else if (PullUpLerp <= 1)
            {
                //set new pull up position
                if (OrigPos.y != LedgePos.y)
                    OrigPos = new Vector3(transform.position.x, LedgePos.y, transform.position.z);


                //move to the ledge position
                float v = (PullUpLerp + 0.5f) * 2;
                float lamt = v;
                transform.position = Vector3.Lerp(OrigPos, LedgePos, PullUpLerp);
            }
            else
            {
                //finished pulling up
                SetOnGround();
            }
        }
    }


    //lerp our current speed to our set max speed, by how much we are pressing the horizontal and vertical input
    void LerpSpeed(float InputMag, float D, float TargetSpeed)
    {
        //multiply our speed by our input amount
        float LerpAmt = TargetSpeed * InputMag;
        //get our acceleration (if we should speed up or slow down
        float Accel = Acceleration;
        if (InputMag == 0)
            Accel = Decceleration;
        //lerp by a factor of our acceleration
        ActSpeed = Mathf.Lerp(ActSpeed, LerpAmt, D * Accel);
    }

    //when in the air or on a wall, we set our action speed to the velocity magnitude, this is so that when we reach the ground again, our speed will carry over our momentum
    void SetSpeedToVelocity()
    {
        float Mag = new Vector2(Rigid.velocity.x, Rigid.velocity.z).magnitude;
        ActSpeed = Mag;
    }

    bool CheckWalls(float X, float Y)
    {
        if (X == 0 && Y == 0) //if no direction input we are not wall running
            return false;

        if (ActWallRunTime >= WallRunTime) //if our wall run timer is more than the amount we can run on walls for, we cannot wall run
            return false;

        //check the collision direction for any walls
        float ClampedY = Mathf.Clamp(Y, 0, 1);
        Vector3 Dir = transform.forward * ClampedY + transform.right * X;

        bool WallCol = Coli.CheckWall(Dir);

        return WallCol;
    }

    void SetInAir()
    {
        OnGroundTimer = 0; //remove the on ground timer
        CurrentState = PlayerStates.InAir;
    }

    void SetOnGround()
    {
        //set our current speed to our velocity
        SetSpeedToVelocity();

        ActWallRunTime = 0; //we are on the ground again, our wall run timer is reset
        InAirTimer = 0; //remove the in air timer
        CurrentState = PlayerStates.Grounded;
    }

    void SetOnWall()
    {
        OnGroundTimer = 0; //remove the on ground timer
        InAirTimer = 0; //remove the in air timer
        CurrentState = PlayerStates.OnWalls;
    }

    void LedgeGrab(Vector3 Ledge)
    {
        //set our ledge position
        LedgePos = Ledge;
        OrigPos = transform.position;
        //reset ledge grab time
        ActPullTm = 0;
        //remove speed and velocity
        Rigid.velocity = Vector3.zero;
        ActSpeed = 0;
        //start ledge grabs
        CurrentState = PlayerStates.LedgeGrab;
    }


    void TurnPlayer(float Hor, float D, float turn)
    {
        //add our inputs to our turn value
        YTurn += (Hor * D) * turn;
        //turn our character
        transform.rotation = Quaternion.Euler(0, YTurn, 0);
    }

    void LookUpDown(float Ver, float D)
    {
        //add our inputs to our look angle
        XTurn -= (Ver * D) * LookUpSpeed;
        XTurn = Mathf.Clamp(XTurn, MinLookAngle, MaxLookAngle);
        //look up and down
        Head.transform.localRotation = Quaternion.Euler(XTurn, 0, 0);
    }

    void MovePlayer(float Hor, float Ver, float D)
    {
        //find the direction to move in, based on the direction inputs
        Vector3 MovementDirection = (transform.forward * Ver) + (transform.right * Hor);
        MovementDirection = MovementDirection.normalized;
        //if we are no longer pressing and input, carryon moving in the last direction we were set to move in
        if (Hor == 0 && Ver == 0)
            MovementDirection = Rigid.velocity.normalized;

        MovementDirection *= ActSpeed;

        //apply Gravity and Y velocity to the movement direction 
        MovementDirection.y = Rigid.velocity.y;

        //apply adjustment to acceleration
        float Acel = DirectionControl * AdjustmentAmt;
        Vector3 LerpVelocity = Vector3.Lerp(Rigid.velocity, MovementDirection, Acel * D);
        Rigid.velocity = LerpVelocity;
    }

    void MoveInAir(float Hor, float Ver, float D)
    {
        //With direction inputs, locate direction to move in
        Vector3 MovementDirection = (transform.forward * Ver) + (transform.right * Hor);
        MovementDirection = MovementDirection.normalized;
        //if we are no longer pressing and input, carryon moving in the last direction we were set to move in
        if (Hor == 0 && Ver == 0)
            MovementDirection = Rigid.velocity.normalized;

        MovementDirection *= ActSpeed;

        //apply Gravity + Y velocity to the movement direction 
        MovementDirection.y = Rigid.velocity.y;

        //lerp movement direction based on air control 
        Vector3 LerpVelocity = Vector3.Lerp(Rigid.velocity, MovementDirection, InAirControl * D);
        Rigid.velocity = LerpVelocity;

    }

    void WallMove(float Ver, float D)
    {
        //get the direction to run up this wall if we press forward + mouse movement (Only works if in range of wall)
        Vector3 MovementDirection = transform.up * Ver;
        MovementDirection *= WallRunUpwardsMovement;

        //forward direction momentum = to our x z velocity 
        MovementDirection += transform.forward * ActSpeed;

        Vector3 LerpVelocity = Vector3.Lerp(Rigid.velocity, MovementDirection, WallRunSpeedAcceleration * D);
        Rigid.velocity = LerpVelocity;
    }

    void JumpUp()
    {
        //only jump if Grounded
        if (CurrentState == PlayerStates.Grounded)
        {
            //reduce our velocity on the y axis to add jump force
            Vector3 VelAmt = Rigid.velocity;
            VelAmt.y = 0;
            Rigid.velocity = VelAmt;
            //add our jump force
            Rigid.AddForce(transform.up * JumpHeight, ForceMode.Impulse);
            //we are now in the air
            SetInAir();
        }
    }

   
 }
