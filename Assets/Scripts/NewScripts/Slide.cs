﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide : MonoBehaviour
{
    [SerializeField]
    float slideHeight;      //What will be the height of our player when we press Slide button
    [SerializeField]
    float slideTime = 2f;   //How much total time for the slide
    [SerializeField]
    float slideSpeed;       //Initial Impulse Speed/Force for Sliding

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();     //Take Rigidbody Reference
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            canSlide = true;                        //If we press C, we can start doing Slide

        if (canSlide)                               //This section will only call when we press C
            StartCoroutine(DoSlide());              //Call the Coroutine "DoSlide"
        else if (shouldReset)                        //After 2 seconds of sliding, reset the player height and speed.
            StartCoroutine(ResetFromSlide());       //Call the Coroutine "ResetFromSlide"
    }

    bool canSlide = false;
    IEnumerator DoSlide()
    {
        //Make the height of player equal to slideHeight(e.g 0.5)
        transform.localScale = new Vector3(transform.localScale.x, slideHeight, transform.localScale.z);

        //Call SlideSelf method
        SlideSelf();

        //Wait for 2 Seconds
        yield return new WaitForSeconds(slideTime);

        //Sliding Should be stopped now. And Resetting Should be started.
        canSlide = false;
        shouldReset = true;
    }

    bool shouldReset = false;
    IEnumerator ResetFromSlide()
    {
        //Reset the player height to 1
        transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);

        //Slowly decrease player velocity from the current velocity to 0
        Vector3 LerpVelocity = Vector3.Lerp(rb.velocity, Vector3.zero, 0.5f);
        rb.velocity = LerpVelocity;

        //After 2 seconds
        yield return new WaitForSeconds(slideTime);
        //Stop Resetting
        shouldReset = false;
    }

    void SlideSelf()
    {
        //find direction
        Vector3 Dir = rb.velocity.normalized;
        Dir.y = 0;

        //slide in direction
        rb.AddForce(transform.forward * slideSpeed * Time.deltaTime, ForceMode.Impulse);
    }
}
