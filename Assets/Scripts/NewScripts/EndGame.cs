﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public GameObject endGameGUI;

    void OnTriggerEnter(Collider col)
	{
        if (col.gameObject.tag == "Player")
            ShowEndGameGUI();
	}

    //Will show the End Game UI when player hits this object.
    void ShowEndGameGUI()
	{
        endGameGUI.SetActive(true);
        endGameGUI.transform.Find("Timer").GetComponent<Text>().text = Timer.totalTime.ToString("0") + "s";
	}

   public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
