using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour
{
    //Whatever scene index you want to load when player hits the object.
    [SerializeField]
    int nextLevelIndex = 2;

    void OnTriggerEnter(Collider col)
    {
        //When player touches this object. Go to nextlevelindex scene.
        if (col.gameObject.tag == "Player")
            SceneManager.LoadScene(nextLevelIndex);
    }
    
}
