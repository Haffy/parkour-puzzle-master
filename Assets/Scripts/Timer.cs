using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    //I've made this variable static. So that it has single instance for all scenes. So that it shifts it's value to the next scene.
    [SerializeField]
    float timeForThisLevel = 10f;
    public static float timeRemaining = 0;
    public static float totalTime = 0;
    public bool timerIsRunning = false;
    public Text timeText;

    private void Start()
    {
        timeRemaining += timeForThisLevel;
        // Starts the timer automatically
        timerIsRunning = true;
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                totalTime += Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                
            }
            {
                if (timeRemaining < 0)
                {
                    timeRemaining = 0;

                    SceneManager.LoadScene("Level1");
                }
               if (Input.GetKeyDown(KeyCode.R))
        {
                    timeRemaining = 0;
                    SceneManager.LoadScene("Level1");

                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    timeRemaining = 0;
                    Timer.totalTime = 0;
                    SceneManager.LoadScene("MainMenu");
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                }

            }
            
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
